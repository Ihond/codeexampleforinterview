import {
  NgStyle,
  NgClass,
  NgTemplateOutlet,
  AsyncPipe,
  NgIf,
  TitleCasePipe,
  DatePipe,
} from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Injector,
  Input,
  OnDestroy,
  Output,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import {
  MonthService,
  PopupOpenEventArgs,
  ScheduleComponent,
  TimeScaleModel,
  View,
  WeekService,
  ScheduleModule,
} from '@syncfusion/ej2-angular-schedule';
import { EventSettingsModel } from '@syncfusion/ej2-schedule';
import { DateTime } from 'luxon';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { map, skipWhile, takeUntil, tap } from 'rxjs/operators';
import { TimeRemainingComponent } from '../../../../../../shared/modules/time-remaining/time-remaining.component';
import { EScheduleType, ICalendarEvents } from './interfaces/interfaces';
import { BaseComponent } from 'src/app/humble/util/classes/base-component';
import { RequestActivitiesService } from 'src/app/util/data-services/request-activities/request-activities.service';
import { PageService } from 'src/app/util/services/page/page.service';

@Component({
  selector: 'app-calendar-view',
  templateUrl: './calendar-view.component.html',
  styleUrls: ['./calendar-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [MonthService, WeekService],
  standalone: true,
  imports: [
    DatePipe,
    TitleCasePipe,
    ScheduleModule,
    NgStyle,
    NgClass,
    TimeRemainingComponent,
    NgTemplateOutlet,
    AsyncPipe,
    NgIf,
  ],
})
export class CalendarViewComponent
  extends BaseComponent
  implements OnDestroy, AfterViewInit
{
  @ViewChild('scheduleObj') public scheduleObj: ScheduleComponent;
  @ViewChild('dateRangeText') dateRangeTextElement: ElementRef;

  @Input() public calendarTemplate: TemplateRef<any>;
  @Output() fetchCalendarData: EventEmitter<any> = new EventEmitter();

  @Input() set data(d: any) {
    this.dataSubject.next(d);
  }
  private _showSidebarSubject = new BehaviorSubject<boolean>(false);
  @Input()
  set showSidebar(value: boolean) {
    this._showSidebarSubject.next(value);
  }

  get showSidebar(): boolean {
    return this._showSidebarSubject.value;
  }

  public selectionTarget: Element;
  public eventAdded: boolean = false;

  private dataSubject: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  public data$: Observable<any[]> = this.dataSubject.asObservable();

  public currentViewSubject: BehaviorSubject<View> = new BehaviorSubject(
    'Month',
  );
  public currentWeekNumberSubject: BehaviorSubject<number> =
    new BehaviorSubject(1);
  private destroy$: Subject<void> = new Subject<void>();

  public timeScaleOptions: TimeScaleModel = { enable: false };
  public colorSubject = new BehaviorSubject<string>(null);
  public color$ = this.colorSubject
    .asObservable()
    .pipe(skipWhile((v) => v === null));

  public startWeek: number = 1;
  public scheduleViews: View[] = ['Week', 'Month'];

  public eventSettings: EventSettingsModel = {
    allowAdding: false,
    editFollowingEvents: false,
    allowEditing: false,
    allowDeleting: false,
    dataSource: [],
  };

  constructor(
    injector: Injector,
    private requestActivitiesSrvc: RequestActivitiesService,
    private pageSrvc: PageService,
  ) {
    super(injector);
  }

  public getFirstAndLastVisibleDay(args: any, eventType: string): void {
    // Get first and last visible days
    const firstVisibleDay: Date = eventType === 'created' ? this.scheduleObj.activeView.renderDates[0] : this.getMonday(args.currentDate);
    const lastVisibleDay = DateTime.fromJSDate(firstVisibleDay)
      .plus({ days: 36 })
      .toUnixInteger();

    this.currentWeekNumberSubject.next(
      +DateTime.fromJSDate(firstVisibleDay).toFormat('W'),
    );

    this.updateCalendarView(
      DateTime.fromJSDate(firstVisibleDay).toUnixInteger(),
      lastVisibleDay,
    );
  }

  public onActionComplete(): void {
    // According to the syncfusion documentation, it's the only one way to add correct translations for html items
    const leftToolbarElement: Element = this.scheduleObj.element.querySelector('.e-toolbar-left .e-date-range .e-tbar-btn .e-tbar-btn-text');
    const monthButton: Element = this.scheduleObj.element.querySelector('.e-toolbar-right .e-month .e-tbar-btn-text');

    if (monthButton) {
      monthButton.innerHTML = this.translate('YearTimeRemaining.month');
    }

    if (leftToolbarElement) {
      combineLatest([this.currentWeekNumberSubject, this.currentViewSubject])
        .pipe(
          tap(([weekNumber, view]) => {
            if (view === EScheduleType.week) {
              leftToolbarElement.innerHTML = `Week ${weekNumber}`;
            }
          }),
          takeUntil(this.destroy$),
        )
        .subscribe();
    }
  }

  public openReadPageOfCurrentItem(data: any): void {
    //Double-click on each item should open new in app tab (not new browser tab, but info component, check screenshots)
    const categoryMap: { [key: string]: string } = {
      '10': 'compliance-activities',
      '5': 'tickets',
      '2': 'tickets',
      '9': 'myp-tasks',
      '11': 'revisions',
      '8': 'workorders',
      '13': 'sustainable-tickets',
      '14': 'sustainable-tasks',
    };

    const category = categoryMap[data.CategoryId];
    if (category) {
      this.pageSrvc.addTab(category, data.id);
    }
  }

  private getMonday(d: Date): Date {
    //To get date correctly, was added approach with 00:00
    //Need to set 00:00 to get values correctly
    d.setHours(0, 0, 0, 0);
    return new Date(
      d.setDate(d.getDate() - d.getDay() + (d.getDay() === 0 ? -6 : 1)),
    );
  }

  private updateCalendarView(firstVisibleDay: any, lastVisibleDay: any): void {
    // Every HTTP request should be handled in parent component,
    // Since child components should not know about the implementation of other methods in parent components
    this.fetchCalendarData.emit({
      firstDay: firstVisibleDay,
      lastDay: lastVisibleDay,
    });
  }

  public getCalendarData(): void {
    // Here we need to restructurize the data to correct format
    this.addSubscription(
      this.data$
        .pipe(
          map((values) => {
            return values?.map((item) => {
              // Uppercase first letter - according the syncfusion documentation
              return {
                id: item.id,
                request_type_id_value: item.request_type_id_value,
                // According to the syncfusion documentation -
                // StartTime and EndTime are needed to show event in specific day ( it will not work without it )
                StartTime: this.getDateByStatus(item),
                EndTime: this.getDateByStatus(item),
                IsAllDay: true,
                Subject: item?.label,
                StatusValue: item?.status_id_label,
                LocationValue: `${item?.location_id_label} - ${item?.location_id_short_label}`,
                CategoryId: item?.request_type_id_value,
                RequestTypeLabel: item?.request_type_id_label,
                Element: `${item?.short_label} - ${item?.label}`,
                TimeRemaining: item.time_remaining,
              };
            });
          }),
        )
        .subscribe({
          next: (items: any) => {
            this.eventSettings = { dataSource: items };
          },
        }),
    );
  }

  ngAfterViewInit() {
    this.getCalendarData();
    // Hot fix for data reload
    this.addSubscription(
      this._showSidebarSubject.subscribe({
        next: () => {
          this.scheduleObj?.refreshLayout();
        },
      }),
    );
  }

  public getDateByStatus(events?: any): Date {
    if (
      events.request_type_id_value === '14' ||
      events.request_type_id_value === '10' ||
      events.request_type_id_value === '9'
    ) {
      return events.expected_start_date;
    } else {
      return events.delivery_date;
    }
  }

  public onClick(): void {
    if (!this.eventAdded) {
      // According to the syncfusion documentation
      const popupInstance: any = (
        document.querySelector('.e-quick-popup-wrapper') as any
      ).ej2_instances[0];
      popupInstance.open = () => {
        popupInstance.refreshPosition();
      };
      this.eventAdded = true;
    }
  }

  public getIconByCategoryId(categoryId: string): any {
    if (categoryId === '10' || categoryId === '13' || categoryId === '14') {
      return this.requestActivitiesSrvc.getIcon(categoryId, 'far fa-tasks');
    }
    if (categoryId === '9' || categoryId === '11') {
      return this.requestActivitiesSrvc.getIcon(
        categoryId,
        'far fa-file-signature',
      );
    }
    if (categoryId === '8') {
      return this.requestActivitiesSrvc.getIcon(
        categoryId,
        'far fa-business-time',
      );
    }
    if (
      categoryId === '2' ||
      categoryId === '3' ||
      categoryId === '4' ||
      categoryId === '5'
    ) {
      return this.requestActivitiesSrvc.getIcon(
        categoryId,
        'far fa-ticket-alt',
      );
    }
  }

  getBackBackgroundColorByCategoryId(remainingTime: any): any {
    const now = DateTime.now();
    let color = ICalendarEvents.whiteColor;
    let backcolor = ICalendarEvents.greenStatusColor;

    if (!remainingTime) {
      return ICalendarEvents.defaultStatusColor;
    }
    if (remainingTime) {
      if (!remainingTime.periods && !remainingTime.caption) {
        return ICalendarEvents.defaultStatusColor;
      }
      remainingTime?.periods?.map((data: any) => {
        const minDate = data.minValue
          ? DateTime.fromJSDate(data.minValue)
          : undefined;
        const maxDate = data.maxValue
          ? DateTime.fromJSDate(data.maxValue)
          : undefined;

        if (maxDate && !minDate) {
          if (now <= maxDate) {
            color = ICalendarEvents.blackColor;
            backcolor = data.backcolor;
          }
        }

        if (maxDate && minDate) {
          if (now <= maxDate && now >= minDate) {
            color = data.color;
            backcolor = data.backcolor;
          }
        }
        if (!maxDate && minDate) {
          if (now >= minDate) {
            color = data.color;
            backcolor = data.backcolor;
          }
        }
      });

      this.colorSubject.next(color);

      return backcolor;
    }
  }

  public onPopupOpen(args: PopupOpenEventArgs): void {
    this.selectionTarget = null;
    this.selectionTarget = args.target;

    // According to the syncfusion support answer.
    // This is the only way how to disable adding event popup when empty scheduler cell is clicked
    if (args.target.classList) {
      const isEmptyCell =
        args.target.classList.contains('e-work-cells') ||
        args.target.classList.contains('e-header-cells');

      if (
        (args.type === 'QuickInfo' || args.type === 'Editor') &&
        isEmptyCell
      ) {
        args.cancel = true;
      }
    }
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.destroy$.next();
    this.destroy$.complete();
  }
}
